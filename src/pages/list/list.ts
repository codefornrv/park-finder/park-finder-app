import { Component } from '@angular/core';
import { NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { IData } from '../../app/data';
import { Park } from '../../app/park';
import { EmptyResults } from '../../app/alert';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  public Data: IData;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, 
    private alertCtrl: AlertController) {

    this.Data = navParams.get('data');

    events.subscribe('filter:updated', this.FilterUpdated);
  }

  private FilterUpdated = () => {
    if (this.Data.Parks.filter(p => p.Visible).length === 0) {
      EmptyResults(this.events, this.alertCtrl);
    }
  };

  ionViewWillLeave() {
    this.events.unsubscribe('filter:updated', this.FilterUpdated);
  }

  SetActivePark(park: Park) {
    this.Data.ActivePark = park;
    this.events.publish('park:selected', park);
  }
}
